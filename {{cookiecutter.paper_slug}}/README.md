# {{cookiecutter.paper_title}}

[![pipeline status](https://gitlab.com/{{cookiecutter.gitlab_username}}/{{cookiecutter.paper_slug}}/badges/master/pipeline.svg)](https://gitlab.com/{{cookiecutter.gitlab_username}}/{{cookiecutter.paper_slug}}/commits/master)

{{cookiecutter.paper_short_description}}

