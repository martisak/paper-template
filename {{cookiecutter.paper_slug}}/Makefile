SOURCES=$(wildcard *.pmd)
# SOURCES=$(subst README.md,,${_SOURCES})

DIR=/usr/local
{% if cookiecutter.template == 'KTH Master Thesis' -%}
TEMPLATE=$(DIR)/kth_thesis/kth-mag.template
{% elif cookiecutter.template == 'Generic article' %}
TEMPLATE=$(DIR)/article/article.template
{% elif cookiecutter.template == 'NeurIPS' %}
TEMPLATE=$(DIR)/pandoc_conference_templates/NeurIPS_2018/neurips_2018.template
{% elif cookiecutter.template == 'ICML' %}
TEMPLATE=$(DIR)/pandoc_conference_templates/ICML_2019/icml_2019.template
{% elif cookiecutter.template == 'ACM CCS' %}
TEMPLATE=$(DIR)/pandoc_conference_templates/acmccs/sigconf.template
{% elif cookiecutter.template == 'IEEE' %}
TEMPLATE=$(DIR)/pandoc_conference_templates/IEEEtran/ieee_conf.template
{% endif %}

LATEXMK=latexmk

{% if cookiecutter.template == 'IEEE' or cookiecutter.template == 'ACM CCS' %}
PDF_ENGINE=pdflatex
LATEXMK_OPTIONS=-bibtex -pdf -pdflatex="pdflatex -interaction=nonstopmode"
{% else %}
PDF_ENGINE=xelatex
{% endif %}

PANDOC_OPTIONS=--table-of-contents \
	--listings \
	--natbib \
	{% if cookiecutter.template == 'ACM CCS' %}-M biblio-style="ACM-Reference-Format" \{% else %}-M biblio-style="IEEEtranN" \
	-M natbiboptions="numbers, sort" \{% endif %}
	-M link-citations=true \
	--include-in-header=header.tex \
	--include-after=appendix.tex \
	--top-level-division=section

# --highlight-style pygments \
# -M fignos-plus-name=Figure \
# -M fignos-cleveref=True \

PANDOC_FILTERS=
#-F pandoc-fignos -F pandoc-citeproc
#--number-sections

PANDOC_REFERENCES=--bibliography=references/refs.bib
# If natbib is not used we can use CSL styles
# --csl=$(DIR)/styles/ieee-with-url.csl

RM = /bin/rm -f
PANDOC=pandoc
PYTHON=python
RSCRIPT=Rscript
PARENTDIR = $(dirname `pwd`)
RMD_OBJECTS=$(SOURCES:.pmd=.Rmd)
MD_OBJECTS=$(SOURCES:.pmd=.md)
PDF_OBJECTS=$(SOURCES:.pmd=.pdf)
PNG_OBJECTS=$(SOURCES:.pmd=.png)
TEX_OBJECTS=$(SOURCES:.pmd=.tex)

DOCKER=docker
DOCKER_IMAGE={{ cookiecutter.docker_image }}
DOCKER_COMMAND=run --rm
DOCKER_MOUNT=-v`pwd`:/data \
		-v `pwd`/references/:/data/references

all: render latex

pdf: $(PDF_OBJECTS) $(TEX_OBJECTS)
tex: $(RMD_OBJECTS) $(MD_OBJECTS) $(TEX_OBJECTS)
png: $(PDF_OBJECTS) $(PNG_OBJECTS)

%.png: %.pdf
	convert \
		-thumbnail "1280x800>" -density 300 \
		-background white -alpha remove $< $@


%.Rmd: %.pmd
	pweave --input-format=markdown \
		--format=pandoc \
		--figure-format=pdf \
		--output=$@ \
		$<

%.md: %.Rmd
	$(RSCRIPT) -e "library(knitr); knit('$<')"

%.pdf: %.tex
	echo $<
	$(LATEXMK) $(LATEXMK_OPTIONS) $<

%.tex: %.md
	echo $<
	$(PANDOC) \
		-f markdown+table_captions  \
		$(PANDOC_FILTERS) \
		--pdf-engine=$(PDF_ENGINE) \
		--template=$(TEMPLATE) \
		$(PANDOC_OPTIONS) \
		$(PANDOC_REFERENCES) -o $@ $<

clean:
	-latexmk -bibtex -C -f $(TEX_OBJECTS)
	-rm $(PDF_OBJECTS) $(TEX_OBJECTS) $(RMD_OBJECTS) $(MD_OBJECTS)

latex:
	$(DOCKER) $(DOCKER_COMMAND) $(DOCKER_MOUNT) $(DOCKER_IMAGE) \
		make -C /data tex

render:
	$(DOCKER) $(DOCKER_COMMAND) $(DOCKER_MOUNT) $(DOCKER_IMAGE) \
		make -C /data pdf

debug:
	$(DOCKER) $(DOCKER_COMMAND) -it $(DOCKER_MOUNT) $(DOCKER_IMAGE) \
		bash

.INTERMEDIATE: $(RMD_OBJECTS) $(MD_OBJECTS)
