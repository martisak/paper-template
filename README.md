# Paper Template in Markdown

A configurable [Cookiecutter](https://github.com/audreyr/cookiecutter) template for scientific papers in Markdown.

## How to use

1. Install `cookiecutter` command line: `pip install cookiecutter`    
2. `cookiecutter gl:martisak/paper-template`
3. Run `make`. 
4. If you are using Gitlab CI, pushing this example will create an artifact.

## You will need

- An idea
- Docker (if run locally).
- A references repository containing your bib-files. I usually add mine as a submodule to this project. An example is provided.
- (Optionally) an environment variable `$GITLAB_API_PRIVATE_TOKEN` to be able
to automatically create a Gitlab private repository.

## You will get

A PDF file with your content. If you have defined the environment variable 
`$GITLAB_API_PRIVATE_TOKEN` then a new Gitlab repository will be created, and
an initial commit will be made and pushed. This automatically renders a 
document using the Gitlab CI pipeline.

Put the following in your startup script, for example `.zshrc`.

```
export GITLAB_API_PRIVATE_TOKEN=[secret]
export GITLAB_API_USERNAME=[username]
export GITLAB_URL=gitlab.com
```

![Template examples](template-0.png) 

## Other

* See [Cookiecutter](https://github.com/audreyr/cookiecutter)  for other templates.
* See [Pandoc Conference Templates](https://github.com/martisak/pandoc_conference_templates) for information on how to use each template.